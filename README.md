# Nginx caching server
![alt text](images/image1.png)

## 1 - What is caching ?
- cache là lớp lưu trữ có tính tạm thời, lưu trữ một tập hợp con dữ liệu truy cập nhanh high-speed ⇒ tăng tốc độ truy cập dữ liệu

## 2 - How does caching work ?
- dữ liệu trong cache thường được lưu trữ trong hardware như RAM ( Random-access memory ) và cũng có thể được sử dụng cùng với một software component
- mục đích chính của cache là tăng hiệu suất truy xuất dữ liệu bằng cách giảm nhu cầu truy cập vào lớp lưu trữ chậm bên dưới

## 3 - Caching with Nginx
Nginx thực hiện caching như thế nào ?
- đầu tiên client gửi một request
![alt text](images/image2.png)

- dựa trên HTTP request => Nginx tạo ra một hash key
![alt text](images/image3.png)

- Bây giờ , Nginx check xem hash key này đã tồn tại trong memory hay chưa , nếu chưa thì request sẽ đi đến application
![alt text](images/image4.png)

- application trả lời request và response sẽ được lưu vào file system
![alt text](images/image5.png)

- hash key được tạo trước đó sẽ được lưu vào trong memory. Để dễ hình dung thì giá trị của hash key và vị trí file lưu nó cùng được lưu trong memory (Nginx chỉ lưu trữ hash key trong memory)
![alt text](images/image6.png)

- cuối cùng user nhận được response
![alt text](images/image7.png)

- khi client requests url giống như trước đó ⇒ Nginx tạo một hash key và check nó có tồn tại trong memory ko và lần này thì hash key đã có trong cache ⇒ Nginx phục vụ file được cache từ file system được liên kết với hash key 
![alt text](images/image8.png)

- request lần thứ 2 này không liên quan gì đến application

## 4 - Debugging the X-Cache-Status header
- MISS : object ko được tìm thấy trong cache. Response được phục vụ từ origin server và có thể đã được lưu vào trong cache
- BYPASS : lấy response từ upstream. Response có thể đã được lưu trong cache
- EXPIRED : object được lưu trong cache đã hết hạn. Response được phục vụ từ upstream
- STALE : object được phục vụ từ cache do origin server response có vấn đề
- UPDATING : phục vụ stale content từ cache do proxy_cache_lock có timed out và proxy_use_stale takes control
- REVALIDATED : proxy_cache_revalidate đã xác minh (verified) nội dung hiện tại được lưu trong cache vẫn còn hợp lệ
- HIT : object được tìm thấy trong cache và nó được phục vụ từ cache

## 5 - Cache Loader
- là quá trình chịu trách nhiệm (responsible) cho loading cache from disk
- chỉ chạy một lần (khi khởi động) và tải siêu dữ liệu vào vùng bộ nhớ và chạy lặp đi lặp lại cho đến khi tất cả các hash key được tải lên hoàn toàn
- điều chỉnh bằng cách sử dụng các lệnh cấu hình sau :
  + loader_threshold : thời gian lặp lại bao lâu
  + loader_files : không load nhiều item hơn
  + loader_sleeps : thời gian tạm dừng giữa các lần lặp lại
- VD : proxy_cache_path /data/nginx/cache keys_zone=one:10m [loader_files=number] [loader_sleep=time] [loader_threshold=time];
![alt text](images/image9.png)

## 6 - Cache Manager
- là tiến trình xóa bộ nhớ cache theo thời gian
- check định kỳ file trong bộ nhớ và xóa dữ liệu ít được sử dụng gần đây nhất nếu kích thước file vượt quá max_size
- xóa các file không được sử dụng độc lập của cài đặt bộ nhớ cache
## 7 - Benefits
- improves site performance
- increases capacity : by reducing load on origin servers
- gives greater availability : by serving stale content when the origin server is down
## 8 - Configure caching server
a - Chuẩn bị
- 1 caching server : 192.168.188.132
- 1 webserver : 192.168.188.134

b - Config
- Caching server
  + cấu hình proxy cache path trong file /etc/nginx/nginx.conf
  ```
    $ sudo vim /etc/nginx/nginx.conf
  ```
  ```bash
    ##
    # Caching config
    ##
    
    proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=cache_zone_name:10m inactive=60m max_size=200m;
  ```
  + cấu hình cache header và … trong file /etc/nginx/sites-enabled/default
  ```
    $ sudo vim /etc/nginx/sites-enabled/default
  ```
  ```bash
    # add cache debugging header
    add_header X-Cache-Status $upstream_cache_status;
    
    # configure cache
    proxy_cache             cache_zone_name;
    proxy_cache_valid       any 1m;
    proxy_cache_key         $scheme$host$request_uri;
  ```
  + bổ sung các directives
  ```bash
    proxy_cache img-cache;
    proxy_cache_valid 200 301 3d;
    proxy_cache_valid any 5s;
    proxy_cache_key "$host$uri$is_args$args";
    proxy_cache_background_update on;
    proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504 invalid_header;
    proxy_cache_revalidate on;
    proxy_cache_min_uses 3;
    proxy_cache_lock on;
    proxy_cache_lock_timeout 60s;
    proxy_cache_lock_age 20s;
  ```
  + giải thích
    * " proxy_cache img-cache; " : đặt tên cho cache
    * " proxy_cache_valid 200 301 3d; " : xác định tgian lưu trữ trong cache là 3 ngày với các mã response 200 , 301
    * " proxy_cache_valid any 5s; " : xác định tgian lưu trữ trong cache là 5 giây với các mã response còn lại chưa được chỉ định trước đó
    * " proxy_cache_key "$host$uri$is_args$args"; " : xác định thông tin để tạo khóa 
    ![alt text](images/image3.png)
    * " proxy_cache_background_update on; " : cập nhật nền (background update) của bộ nhớ đệm từ origin server mà không chặn yêu cầu từ người dùng
    * " proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504 invalid_header; " : danh sách các mã lỗi và điều kiện khi sử dụng dữ liệu cũ từ cache khi origin server không thể cung cấp dữ liệu mới
    * " proxy_cache_revalidate on; " : gửi yêu cầu kiểm tra tính hợp lệ thông tin trong cache và trên origin server
    * " proxy_cache_min_uses 3; " :  cần ít nhất 3 yêu cầu từ người dùng khác nhau ( yêu cầu đến cùng một tài nguyên từ backend server ) trước khi một phản hồi được lưu trữ trong cache
    * " proxy_cache_lock on; " : kiểm soát quá trình cập nhật và truy xuất dữ liệu trong cache
    * " proxy_cache_lock_timeout 60s; " : xác định thời gian tối đa mà một yêu cầu có thể chờ để giữ khóa cache trước khi bị hủy bỏ
    * " proxy_cache_lock_age 20s; " : xác định thời gian tối thiểu mà một khóa cache phải tồn tại trước khi có thể bị tái sử dụng hay tái cấp phát cho các yêu cầu khác
  + tạo file /var/cache/nginx để lưu các file cache
  ```
    $ sudo mkdir /var/cache/nginx
    $ sudo service nginx restart
    $ sudo service nginx status
  ```

- Webserver
  + thay đổi quyền truy cập trong thư mục /var/www/html/
  ```
    $ chmod 775 /var/www/html
  ```
  + add file anh.png

c - Test (Vào trình duyệt và bấm F12 sang phần network)
- 192.168.188.100/
  + request lần 1 
  ![alt text](images/image11.png)
  + kiểm tra file /var/cache/nginx xem nó đã tạo file cache chưa
    ![alt text](images/image12.png)
  + request lần 2
  ![alt text](images/image13.png)
  + kiểm tra file /var/cache/nginx 
  ![alt text](images/image14.png)
  + request lần 3 
  ![alt text](images/image15.png)
  + request lần 4 
  ![alt text](images/image16.png)

- 192.168.188.100/anh.png
  + requets lần 1 
  ![alt text](images/image17.png)
  + kiểm tra file /var/cache/nginx xem nó đã tạo file cache chưa
  ![alt text](images/image18.png)
  + request lần 2 
  ![alt text](images/image19.png)
  + kiểm tra file /var/cache/nginx
  ![alt text](images/image20.png)
  + request lần 3 
  ![alt text](images/image21.png)

